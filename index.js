var infopack = require('infopack');
var ymlToJson = require('sbp-utils-yml-to-json');

/**
 * Load generators below
 */
// var infopackGenMarkdownToHtml = require('infopack-gen-markdown-to-html');

/**
 * Create a new pipeline
 */
var pipeline = new infopack.Pipeline();

// generate json file from yamls
pipeline.addStep(ymlToJson.step({
	// omitFile: true,
	readPath: './raw_files/methods'
}));

/**
 * Run pipeline
 */
pipeline
    .run()
    .then((data) => {
        return pipeline.generateStaticPage();
    })
    .then(() => {
        console.log(pipeline.prettyTable());
    })
    .catch((err) => {
        console.error(err);
    });
