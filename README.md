# SBP Metoder - Öppna metodbeskrivningar

Detta projekt är ett initiativ för att samla och harmonisera metodbeskrivningar. Det finns inga begräsningsar för vilka metodbeskrivningar som är lämpliga att lagra här men listan har ett naturligt fokus på samhällsbyggnad.

## Webbplats

Listan finns publicerad på http://nrb.sbplatform.se

## Förvaltare

Vi som förvaltar detta projekt är:

* [Johan Asplund](https://www.linkedin.com/in/bimjohan)
* [Rogier Jongeling](https://www.linkedin.com/in/rogierjongeling/)

Saknas ditt namn? Skapa ett ändringsförslag [här](https://gitlab.com/swe-nrb/sbp-metoder/edit/master/README.md)

## Stadgar

Detta informationspaket förvaltas enligt nedanstående stadgar.

### Förvaltningsprocessen

Alla metodbeskrivningar är välkomna, förvaltningsgruppen vädjar dock om att hålla listan "samhällsbyggnadsfokuserad". Det finns idag tre kravnivåer för metodbeskrivningar, den första nivån är tekniska krav för att metodbeskrivningen överhuvudtaget ska komma in i listan och de två andra nivåerna är kvalitetskrav med syfte att erbjuda konsumenterna ett så bra material som möjligt.

Den lägre nivån måste alltid vara uppfylld för att kunna gå vidare till nästa nivå.

**1. Tekniska krav**

1. Metodbeskrivningen lagras som en fil enligt senast gällande [schema](https://gitlab.com/swe-nrb/sbp-metoder/blob/master/support_files/schemas/method_v1.schema.yml) (kallat metodbeskrivningsfilen).
2. Metodbeskrivningsfilen lagras i mappen `/raw_files/methods` i detta informationspaket.
3. Metodbeskrivningsfilen är en korrekt [YAML-fil](http://yaml.org/).

**2. Kvalitetssäkrad**

1. `Text saknas här`
3. Fälten `definition` och `comment` skrivs som [fullständiga meningar](http://www.prefix.nu/fullstandig-mening.html).
4. Fältet `definition` ska ensamt kunna ge tillräcklig kontext för att definiera metodbeskrivningen.
    * Fältet `comment` ska alltså inte behöva läsas för att förstå metodbeskrivningens innebörd.
5. Fältet `comment` används för att ge läsaren värdefull indirekt information om metodbeskrivningen.
6. Källan i fältet `source` ska vara relevant och allmänt känd
7. Ev. länkar ska vara fungerande och får inte peka till "låsta" webbplatser
8. Metodbeskrivningen får inte vara förvirrande gentemot redan kvalitetssäkrade metodbeskrivningar
    * Om en snarlik metodbeskrivningen redan finns definierad ska den nya definition tydligt urskilja sig (vi vill inte ha flera metodbeskrivningar som egentligen betyder samma sak). Notera att det dock är okej ur ett kvalitetsperspektiv att ha olika defitioner med t.ex. olika källar (det är rimligt att två olika källar t.ex. har en metodbeskrivning hur man namnger filer)

**3. NRB Rekommendation**

NRB rekommendationen är ett sätt att tydligt markera vilka metodbeskrivningar som är lämpliga att använda i samhällsbyggnadssektor. Rekommendationerna bygger på subjektiva åsikter och det är därför viktigt att metodbeskrivningarna erhåller nivån via en transparent process och att motivieringar dokumenteras för förståelse så att så hög acceptans som möjligt kan nås.

1. Metodbeskrivningar som är särkilt relevanta för samhällsbyggnadssektorn ska få en rekommendation.
2. I det fall ett metodbeskrivning erhåller en rekommendation gentemot liknande metodbeskrivningar (som avhandlar närliggande/samma ämne) ska en tydlig motivering dokumenteras.
3. En rekommenderat metodbeskrivning måste tillgodose behovet hos en majoritet av branschen.
    * t.ex. genom att en beredning gjorts med ett öppet remissförfarande

### Metodfil  

Du kan lägga till dina egna metoder genom att skapa en metodfil i mappen
`/raw_files/methods`. För att föreslå metoder måste du vara inloggad, om du inte har ett gitlab konto så kan du skapa det [här](https://gitlab.com/users/sign_in) (det är kostnadsfritt).

**Mall**  

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-metoder/raw/master/support_files/schemas/method_v1.schema.yml) under mappen `/support_files/schemas`.

### Taggfil

Metoder kan "dekoreras" med taggar för att påvisa vissa egenskaper på metoderna (t.ex. om metoden är kvalitetsgranskat).
En tagg definieras genom att skapa en mapp med taggens namn och lägga in en `definition.yml`
med bl.a. information om godkännandeprocessen för att ett begrepp ska kunna få taggen.

Taggmappen läggs i mappen `/raw_files/tags`.

**Mall**

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-metoder/raw/master/support_files/schemas/tag_v1.yml) under mappen `/support_files/schemas`.

## Namngivning av filer

Ange metoder som filnamn med datorvänliga tecken:

* a-z
* 0-9
* \- (dash) | _ (underscore) | , (comma)

__Exempel__

anlaggningsmodell-trafikverket.yml

## Steg-för-steg guider  

### Ansök om medlemskap (behövs för att lämna förbättringsförslag)  

1. Logga in på Gitlab
2. Ansök om medlemskap genom att klicka på request access under [projektets sida](https://gitlab.com/swe-nrb/sbp-metoder)
3. Direkt du har blivit godkänd kan du följa stegen under bidra

Alternativt kan du [forka](https://docs.gitlab.com/ee/workflow/forking_workflow.html) projektet.

### Lägg till metoder genom att skapa egna filer

1. Kopiera mallen [LÄNK](https://gitlab.com/swe-nrb/sbp-metoder/raw/master/support_files/schemas/method_v1.schema.yml)
2. Skapa en ny fil under mappen underlag [LÄNK](https://gitlab.com/swe-nrb/sbp-metoder/new/master/raw_files/methods?file_name=filename.yml) (om du inte är inloggad eller har ansökt om medlemskap så kommer du till en felsida)
3. Benämna filen enligt ovan (__OBS!__ Ange benämningen utan åäö och med små bokstäver, avsluta med filändelsen `.yml`, t.ex. "anlaggningsmodell.yml". Filnamn ska vara exakt samma som fältet `filename` i mallfil)
4. Spara som en ny utvecklingsgren (skapa ny "branch")
5. Skapa en ny ändringsbegäran (skapa en "merge request" från din nya utvecklingsgren till "master")
6. Förslag bearbetas av metodlistans förvaltningsgrupp
